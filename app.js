var express = require('express')
  , fs = require('fs')
  , passport = require('passport')
  , util = require('util')
  , BasicStrategy = require('passport-http').BasicStrategy
  , session = require('express-session')
  , bodyParser = require("body-parser")
  , cookieParser = require("cookie-parser")
  , methodOverride = require('method-override')
  , fb = require('fbgraph')
  , cron = require('cron')
  , async =require('async')
  , fs = require('fs')
  , mysql = require('mysql')
  , database = require('./database')
  ;

var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
var puzzles = JSON.parse(fs.readFileSync('treasure-hunt-blr.json', 'utf8'));
var users = JSON.parse(fs.readFileSync('valid-pass.json', 'utf8'));
var auth_dict = {};
console.log(users);
for (var i = 0; i < users.length; ++i) {
  auth_dict[users[i].user] = users[i].pass;
}

console.log(auth_dict);

var query1 = 'DROP TABLE users';
database.executeQuery(query1, [], function(err, result) {
    if (err) {
      console.log(err);
    }
});

var query = 'CREATE TABLE users (id VARCHAR(20) PRIMARY KEY, level INT(6) DEFAULT 0, lives INT(6) DEFAULT 0, hint INT(6) DEFAULT 0, blocked INT(6) DEFAULT 0, time INT(20) DEFAULT 0)'
database.executeQuery(query, [], function(err, result) {
    if (err) {
      console.log(err);
    }
});

var app = express();
// configure Express
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
//app.use(logger());
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(methodOverride());
// Initialize Passport!  Also use passport.session() middleware, to support
// persistent login sessions (recommended).
app.use(session({ secret: 'anything' }));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(__dirname + '/public'));




var FACEBOOK_APP_ID = config.fb_app_id;
var FACEBOOK_APP_SECRET = config.fb_app_secret;

app.use(function(req, res, next){                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
  var err = req.session.error                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
    , msg = req.session.success;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
  delete req.session.error;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
  delete req.session.success;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
  res.locals.message = '';                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
  if (err) res.locals.message = '<p class="msg error">' + err + '</p>';                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
  if (msg) res.locals.message = '<p class="msg success">' + msg + '</p>';                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
  next();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
});     

app.get('/', function(req, res){
  console.log(req.session.user);
  if (req.session.user) {
    database.getUser(req.session.user.id, function(err, result1) {
      if (err) {
        console.log(err);
        res.render('error');
      } else if (result1.length == 0) {
        database.insertUser(req.session.user.id, function(err, result2) {
          if (err) {
            res.render('error');
          } else {
            console.log('Result 2');
            console.log(result2);
            res.render('index2', { user: req.session.user, lives: 0});
          }
        });
      } else {
        console.log('Result 1');
        console.log(result1);
        res.render('index2', { user: req.session.user, lives: result1.lives});
      }
    });
  } else {
    res.render('index2', { user: req.user, lives: req.lives});
  }
});


app.get('/correct', function(req, res){
  res.render('correct');
});

app.get('/wrong', function(req, res){
  res.render('wrong');
});

app.get('/game', function(req, res){
  if (req.session.user) {
    // console.log(req.user.id);
    database.getUser(req.session.user.id, function(err, result1) {
      if (err) {
        console.log('Error 1');
        console.log(err);
        res.render('error');
      } else {
        console.log(result1);
        console.log(puzzles[0]);
        //var index = Integer.parseInt(result1);
        console.log(result1[0].hint);
        if (result1[0].hint == 0) {
          res.render('game', { image: puzzles[result1[0].level].img , puzzle : puzzles[result1[0].level].puzzle, level : result1[0].level.toString(), hint : 0 });
        } else {
          res.render('game', { image: puzzles[result1[0].level].img , puzzle : puzzles[result1[0].level].puzzle, level : result1[0].level.toString(), hint : puzzles[result1[0].level].hint});
        }
      }
    });
  } else {
    console.log(err);
    console.log('Error 2');
    res.render('error');
  }
});

app.post('/submit', function(req, res){
  console.log(req.body.answer);
  console.log(req.body.level);
  database.getUser(req.session.user.id, function(err, result1) {
      var level = parseInt(req.body.level);
      if (err) {
        console.log('Error 1');
        console.log(err);
        res.render('error');
      } else if (result1[0].level != level) {
        console.log('Error 2');
        console.log(err);
        res.render('error', {error: "This is not your current level"});
      } else {
        if (puzzles[result1[0].level].answer == req.body.answer) {
          var date = new Date();
          var timestamp = date.getTime();
          database.updateUser(req.session.user.id, 1, 0, 0, timestamp, function(err, result) {
            if (err) {
              console.log('Error 2');
              console.log(err);
              res.render('error', {error: "Could not update"});
            } else {
              res.redirect('/correct');
            }
          });
        } else {
          database.updateUser(req.session.user.id, 0, 0, 1, -1, function(err, result) {
            if (err) {
              res.render('error', {error: "Could not Update"});
            } else {
              res.redirect('/wrong');
            }
          });
        }
      }
  });

});

app.post('/hint', function(req, res){
  console.log(req.body.answer);
  console.log(req.body.level);
  console.log('Post request made to hint');
  database.getUser(req.session.user.id, function(err, result1) {
      var level = parseInt(req.body.level);
      if (err) {
        console.log('Error 1');
        console.log(err);
        res.render('error');
      } else if (result1[0].level != level) {
        console.log('Error 2');
        console.log(err);
        res.render('error', {error: "This is not your current level"});
      } else {
        if (result1[0].hint == 0) {
          database.updateUser(req.session.user.id, 0, 1, 5, -1, function(err, result) {
            if (err) {
              console.log('Error 2');
              console.log(err);
              res.render('error', {error: "Could not update"});
            } else {
              res.send('right');
            }
          });
        } else {
          res.send('right');
        }
      }
  });
});

app.get('/login', function(req, res){
  res.render('login');
});

app.post('/login', function(req, res){
  if (auth_dict[req.body.user] == req.body.pass) {
    req.session.regenerate(function(){                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
      // Store the user's primary key                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
      // in the session store to be retrieved,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
      // or in this case the entire user object                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
      req.session.user = {};
      req.session.user.id = req.body.user;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
      res.redirect('/');                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
    });   
  } else {
    req.session.error = 'Authentication failed, please check your '                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
      + ' username and password.'                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
      + ' (use "tj" and "foobar")';                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
    res.redirect('/login');  
  }
});
 

app.get('/logout', function(req, res){
  req.session.destroy(function(){                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
    res.redirect('/');                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
  });
});

app.listen(config.port);

// Simple route middleware to ensure user is authenticated.
//   Use this route middleware on any resource that needs to be protected.  If
//   the request is authenticated (typically via a persistent login session),
//   the request will proceed.  Otherwise, the user will be redirected to the
//   login page.
function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login')
}